#define HEAP_SIZE 4096
#define MEMORY_SIZE 4096

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdlib.h>

void test_memory_allocate();

void test_memory_free_one_block();

void test_memory_free_with_two_blocks();

void test_memory_allocate_with_small_heap_size();

void test_memory_allocate_region_was_create_in_another_place();
